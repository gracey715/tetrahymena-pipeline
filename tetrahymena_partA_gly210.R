# Read in data from file
# path <- '/Users/graceyang/Documents/tetrahymena.tsv'
# tetrahymena <- read_tsv(path)

# Filter out excessively small cells
# filter(tetrahymena, diameter>19.2)
# Group by culture and glucose
# grouped <- group_by(tetrahymena, culture, glucose)
# Use mean concentration and mean diameter over replicates
# summed <- summarise(grouped, mconc = mean(conc, na.rm = TRUE), mdiam = mean(diameter, na.rm = TRUE))
# Transform mean concentration and mean diameter using natural log
# transformed <- mutate(summed, log_conc = log(mconc), log_diameter = log(mdiam))
# Plot using ggplot
# Different color and shape for glucose yes or no
# plotted <- ggplot(data = transformed, mapping = aes(x=log_conc, y=log_diameter)) + geom_point(data = transformed, aes(color = glucose, shape = glucose)) + geom_smooth(data = transformed, method="loess", aes(color = glucose))
# Save the resulting plot
# ggsave("tetrahymena_partA_gly210.pdf")

# There is a linear trend between log concentration vs. log diameter.
# As log concentration increases, log diameter decreases.

# In pipeline format
final <- tetrahymena %>% filter(diameter>19.2) %>% group_by(culture, glucose) %>% summarise(mconc = mean(conc, na.rm = TRUE), mdiam = mean(diameter, na.rm = TRUE)) %>% mutate(log_conc = log(mconc), log_diameter = log(mdiam))

plotted <- ggplot(data = transformed, mapping = aes(x=log_conc, y=log_diameter)) + geom_point(data = transformed, aes(color = glucose, shape = glucose)) + geom_smooth(data = transformed, method="loess", aes(color = glucose))

ggsave("tetrahymena_partA_gly210.pdf")