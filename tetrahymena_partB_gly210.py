import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# Read in data
path = "/Users/graceyang/Documents/tetrahymena.tsv"
tetrahymena = pd.read_table(path, delim_whitespace = True)
# Filter out cells with diameter <= 19.2
filtered = tetrahymena.loc[tetrahymena['diameter'] > 19.2]
# Group by culture and glucose
# Take mean concentration and mean diameter
grouped = filtered.groupby(['culture', 'glucose'])['conc','diameter'].mean().reset_index()
# Transform mean concentration and diameter by natural log
grouped['log_conc'] = np.log(grouped['conc'])
grouped['log_diameter'] = np.log(grouped['diameter'])

# Create scatter plot
plotted = sns.lmplot(x='log_conc', y='log_diameter', data=grouped,
                     hue='glucose', markers = ['o','x'], legend=False)
plt.legend(loc='upper right')
plotted.savefig('tetrahymena_partB_gly210.pdf')
